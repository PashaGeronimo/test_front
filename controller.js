'use strict';
class App {
    constructor() {
        this.backUrl = 'http://localhost:8080';
        this.clickHandler();
        this.loadData();
        this.token = '';
    }

    send(url, type, body, form) {
        let that = this;
        return new Promise(function (resolve, reject) {
        let xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                resolve(that.dataParse(xmlHttp.responseText) );
            } else if (xmlHttp.readyState === 4 && xmlHttp.status === 405) {
                console.log(that.dataParse(xmlHttp.responseText) );
                alert( that.dataParse(xmlHttp.responseText).error);
            }

            if(this.readyState === this.HEADERS_RECEIVED) {
                if (xmlHttp.getResponseHeader("authorization"))  that.token = xmlHttp.getResponseHeader("authorization");

            }
        };
        xmlHttp.open(type, that.backUrl + url, true);
        if (form) {
            xmlHttp.send(body);
        }
         else  {
             if (that.token)  {
                 xmlHttp.setRequestHeader("authorization", that.token);
             };
            xmlHttp.setRequestHeader("Content-type", "application/json");
            xmlHttp.send(JSON.stringify(body));
        }


        });
    }


    loadData () {
        let that = this;
        let table;
        that.send('/user/free', 'GET').then(
            async response => {
                table = await that.gui(response);

                $('#apnd').html(table);
            })

    }


    loadMyData () {
        let that = this;
        let table;
        that.send('/user/my', 'GET').then(
            async response => {
                table = await that.gui(response);
                $('#apnd2').html(table);


            });

    }


    clickHandler() {
        let that = this;

        $("#register").click(function (event) {
            event.preventDefault();
            let body = {
                email:$('#email').val(),
                password: $('#password').val()
            }
            that.send('/user/register', 'POST', body).then(
                response => {
                    alert('Has been registred');
                    $(':input').val('');
                })
        });


        $("#login").click(function (event) {
            event.preventDefault();
            let body = {
                email:$('#email').val(),
                password: $('#password').val()
            }
            that.send('/user/login', 'POST', body).then(
                response => {
                    alert('Has been logined');
                    $(':input').val('');
                    $('#logreg').hide();
                    $('#logined').show();
                    $('#free').show();
                })
        });

        $("#logout").click(function (event) {
            event.preventDefault();
                    that.token = '';
                    $('#logreg').show();
                    $('#logined').hide();
                    $('#youbook').hide();
                    $('#free').hide();
        });

        $("#mybook").click(function (event) {
            event.preventDefault();
            let table;
            that.loadMyData();
            $('#free').hide();
                    $('#youbook').show();

        });

        $("#freebook").click(function (event) {
            event.preventDefault();
            that.loadData();
            $('#free').show();
            $('#youbook').hide();


        });



        $("#getbook").click(async function (event) {
            event.preventDefault();
            let books = [];
            await $('input:checked').each(function () {
                if ( $(this).attr('name'))  books.push($(this).attr('name'))
            });
            that.send('/user/take', 'POST', {books:books}).then(
                response => {
                    console.log('updated');
                    that.loadData ();
                })


        });

        $("#return").click(async function (event) {
            event.preventDefault();
            let books = [];
            await $('input:checked').each(function () {
                if ( $(this).attr('name'))  books.push($(this).attr('name'))
            });
            that.send('/user/return', 'POST', {books:books}).then(
                response => {
                    // console.log('updated');
                    that.loadMyData();
                })
        });

    }



    async gui(response) {
        let that = this;
        let table='';
        response.Books = response.Books.reverse();
        response.Books.forEach(function (book) {
            let cover = '';
            let firstTd = '<td rowspan="'+  book.Authors.length  + '">';
            if (book.photo) cover =  '<img src="' + that.backUrl +'/book/photo/' + book.photo +' " /></td>';
            if (book.Authors.length === 0) {
                firstTd = '<td>';
            }
            cover = firstTd + cover ;
            table = table + '<tr>' + firstTd + book.name + '<br/><input class="ch" type="checkbox" name="' +  book.id + '"  />   </td>';

            if (book.Authors.length !== 0) {
                book.Authors.forEach(function (author, i) {
                    if (i === 0 ) {
                        table = table + '<td>' + author.firstName + '</td><td>' + author.lastName + '</td>' + cover  + '</tr>';
                    } else {
                        table = table + '<tr><td>' + author.firstName + '</td><td>' + author.lastName + '</td></tr>';
                    }


                });
            } else {
                table = table  + '<td></td><td></td>' + cover  + '</tr>';
            }
        });
        return table;
    }



    dataParse(data) {
        return JSON.parse(data);
    }



}




let app1 = new App();
